﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelativeMovement : MonoBehaviour {

	public Transform target;
	public float PlayerSpeed=5.0f;
	public float JumpSpeed=20.0f;
	public float rotSpeed = 15.0f;

	private CharacterController _controller;
	private Transform _myTransform;
	Vector3 moveDir=Vector3.zero;

	void Awake(){
		_controller = GetComponent<CharacterController> ();
	}

	// Use this for initialization
	void Start () {
		_myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {

		float horInput = Input.GetAxis ("Horizontal");
		float vertInput = Input.GetAxis ("Vertical");

		if (horInput != 0 || vertInput != 0) {
			moveDir.x = horInput;
			moveDir.z = vertInput;
		}

		Quaternion temp = target.rotation;

		target.eulerAngles = new Vector3 (0.0f, target.eulerAngles.y, 0.0f);

		moveDir = target.TransformDirection (moveDir);

		target.rotation = temp;

		Quaternion direction = Quaternion.LookRotation (moveDir);

		transform.rotation = Quaternion.Lerp (transform.rotation, direction, Time.deltaTime * rotSpeed);

		_controller.Move (moveDir * Time.deltaTime * PlayerSpeed);
	}
}
